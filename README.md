# Distribution Logos
#### Suspicious logos for various openSUSE distributions
The logos are originally from: https://github.com/openSUSE/distribution-logos

The Github repository itself doesn't have any license, but the [specfile](https://build.opensuse.org/package/view_file/openSUSE:Factory/distribution-logos-openSUSE/distribution-logos-openSUSE.spec?expand=1) indicates that the original logos are licensed under CC-BY-SA-4.0 so these logos are also licensed under CC-BY-SA-4.0.
This repo is unofficial and not associated with the openSUSE project, it is just a shitpost.

light-inline: 44px tall, variable width, svg and png
light-dual-branding 180x80px, svg
square-hicolor 128px, svg
square-symbolic 16px, svg
favicon multiple sizes, ico
